FROM openjdk:8
MAINTAINER lucifer lu_ciffier@hotmail.com
VOLUME /tmp
ADD target/webapp-1.0-SNAPSHOT.jar  webapp.jar
EXPOSE  8443
ENTRYPOINT    ["java","-jar","/webapp.jar"]
