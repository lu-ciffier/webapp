#/bin/bash
#ENV
 #docker私有仓库（Harbor）
  registryUrl=192.168.1.110:5000
  registry_user="xxx"
  registry_pass="xxx"
  project_name=$1
  image_name=$2
  env=$3
  node_user=root
if [ "${env}"  == test ];then
   #测试环境
  node1=192.168.1.105
elif [ "${env}" == prod ];then
  #生产环境
  node1=192.168.1.106
else
    echo '没有${env}环境！！！'
fi
  
#Prepare
echo "project_name: $1 , image_name: $2 , env:$3"

#Deploying
    #可以写成一个ssh＂＂，为了解析方便，切分开（接下来为第一行，以此类推）
    ssh $node_user@$node1 "docker login --username=${registry_user} --password=${registry_pass} ${registryUrl} “
    ssh $node_user@$node1 “docker pull $image_name && docker rm -f $project_name || true ”
    ssh $node_user@$node1 ”docker run -itd --name=$project_name --restart=always --net=host -e TZ="Asia/Shanghai" -P -v /data/logs/$project_name:/data/logs/$project_name $image_name“
    ssh $node_user@$node1 “docker image prune -a -f --filter 'until=1h' ”
    ssh $node_user@$node1 ”tailf /data/logs/$project_name/${project_name}.log"
